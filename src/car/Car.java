package car;

import java.util.Arrays;

public class Car {
    private String color;
    private String brand;
    private String manufacter;
    private String model;
    private int year;
    private String seatColor;


private Engine engine;
    private Wheel[] wheel;
private Door[] door;

    public Car(String color, String brand, String manufacter, String model, int year, String seatColor, Engine engine, Wheel[] wheel, Door[] door)
    throws Exception{

        if (wheel.length <3) {
            throw new Exception("Does not a car");
        }
        if (door.length < 2) {
            throw new Exception("Not a car");
        }



        this.color = color;
        this.brand = brand;
        this.manufacter = manufacter;
        this.model = model;
        this.year = year;
        this.seatColor = seatColor;
        this.engine = engine;
        this.wheel = wheel;
        this.door = door;
    }



    @Override
    public String toString() {
        return "Car{" +
                "color='" + color + '\'' +
                ", brand='" + brand + '\'' +
                ", manufacter='" + manufacter + '\'' +
                ", model='" + model + '\'' +
                ", year=" + year +
                ", seatColor='" + seatColor + '\'' +
                ", engine=" + engine +
                ", wheel=" + Arrays.toString(wheel) +
                ", door=" + Arrays.toString(door) +
                '}';
    }
}
