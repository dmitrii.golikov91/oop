package car;

public class Rim {
    private float radius;
    private float width;
    private String color;
    private String rimType;

    public Rim(float radius, float width, String color, String rimType) {
        this.radius = radius;
        this.width = width;
        this.color = color;
        this.rimType = rimType;
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getRimType() {
        return rimType;
    }

    public void setRimType(String rimType) {
        this.rimType = rimType;
    }

    @Override
    public String toString() {
        return "Rim{" +
                "radius=" + radius +
                ", width=" + width +
                ", color='" + color + '\'' +
                ", rimType='" + rimType + '\'' +
                '}';
    }
}
