package car;

public class Window {
    private float tintedPercentage;
    private String color = "black";
    private Shape shape;

    public Window(float tintedPercentage, Shape shape) {
        this.tintedPercentage = tintedPercentage;
        this.shape = shape;
    }

    public float getTintedPercentage() {
        return tintedPercentage;
    }

    public void setTintedPercentage(float tintedPercentage) {
        this.tintedPercentage = tintedPercentage;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Shape getShape() {
        return shape;
    }

    public void setShape(Shape shape) {
        this.shape = shape;
    }

    @Override
    public String toString() {
        return "Window{" +
                "tintedPercentage=" + tintedPercentage +
                ", color='" + color + '\'' +
                ", shape=" + shape +
                '}';
    }
}

