package car;

public class Door {
    private Window window;
    private String color;
    private String openingType;
    private Shape shape;

    public Door(Window window, String openingType, Shape shape) {
        this.window = window;
        this.openingType = openingType;
        this.shape = shape;
    }

    public Window getWindow() {
        return window;
    }

    public void setWindow(Window window) {
        this.window = window;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getOpeningType() {
        return openingType;
    }

    public void setOpeningType(String openingType) {
        this.openingType = openingType;
    }

    public Shape getShape() {
        return shape;
    }

    public void setShape(Shape shape) {
        this.shape = shape;
    }

    @Override
    public String toString() {
        return "Door{" +
                "window=" + window +
                ", color='" + color + '\'' +
                ", openingType='" + openingType + '\'' +
                ", shape=" + shape +
                '}';
    }
}
