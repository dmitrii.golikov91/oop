package car;

public class Shape {
    private float width;
    private float height;
    private float shape;

    public Shape(float width, float height) {
        this.width = width;
        this.height = height;
        shape = width*2+height*2;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public float getShape() {
        return shape;
    }

    @Override
    public String toString() {
        return "Shape{" +
                "width=" + width +
                ", height=" + height +
                ", shape=" + shape +
                '}';
    }
}
