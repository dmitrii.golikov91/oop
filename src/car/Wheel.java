package car;

public class Wheel {
    private float radius;
    private float width;
    private String color = "black";
    private Rim rim;

    public Wheel(float radius, float width, Rim rim) {
        this.radius = radius;
        this.width = width;
        this.rim = rim;
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Rim getRim() {
        return rim;
    }

    public void setRim(Rim rim) {
        this.rim = rim;
    }

    @Override
    public String toString() {
        return "Wheel{" +
                "radius=" + radius +
                ", width=" + width +
                ", color='" + color + '\'' +
                ", rim=" + rim +
                '}';
    }
}
